---
marp: true
---

# Directives

---

`v-bind` Connects an attribute in an HTML tag to a data variable inside the Vue instance. (`:`)

`v-if` Creates HTML tags depending on a condition. Directives `v-else-if` and `v-else` are used together with the v-if directive.

`v-show` Specifies if an HTML element should be visible or not depending on a condition.

`v-for` Creates a list of tags based on an array in the Vue instance using a for-loop.

`v-on` Connects an event on an HTML tag to a JavaScript expression or a Vue instance method. We can also define more specifically how our page should react to a certain event by using event-modifiers.(`@`)

`v-model` Used in HTML forms with tags like <form>, <input> and <button>. Creates a two way binding between an input element and a Vue instance data property.(`:`)
[source: W3school](https://www.w3schools.com/vue/vue_directives.php)
[Full list on vuejs.org](https://vuejs.org/api/built-in-directives.html)

---

## v-bind (:)

```js
<script setup>
  import {ref} from 'vue';

  const msg = ref('some info here.');
  const customId = "myCustomId";

  function changeMsg() {
    msg.value = "msg changed";
  }
</script>

<template>
  <p :id="customId">Info: {{ msg }}</p>
  <button @click=changeMsg>change</button>
</template>
```

---

## v-if else

```js
<script setup>
  import {ref} from 'vue';

  const isLoading = ref(true);

  function switchLoading() {
    isLoading.value = !isLoading.value;
  }
</script>

<template>
  <p v-if="isLoading">Loading ...</p>
  <p v-else>Data Loaded</p>
  <button @click=switchLoading>change</button>
</template>
```

---

## v-show

```js
<script setup>
  import {ref} from 'vue';

  const isLoading = ref(true);

  function switchLoading() {
    isLoading.value = !isLoading.value;
  }
</script>

<template>
  <p class="loading-data" v-if="isLoading">Loading ...</p>
  <p v-else>Data Loaded</p>
  <button @click=switchLoading>change</button>
</template>

<style scoped>
  .loading-data {
    background: 'red';
    padding: 8px;
  }
</style>
```

---

## v-for

```js
<script setup>
  const datas = [
    {id: 1, name: 'test1'},
    {id: 2, name: 'test2'},
    {id: 3, name: 'test3'},
  ]
</script>

<template>
  <p v-for="data in datas" :key="data.id">{{data.name}}</p>
</template>
```

---

## v-on (@)

```js
<script setup>
  import {ref} from 'vue';

  const datas = ref([
    {id: 1, name: 'test1'},
    {id: 2, name: 'test2'},
    {id: 3, name: 'test3'},
  ]);

  function reverse() {
    datas.value = datas.value.reverse();
  }
</script>

<template>
  <--! <button v-on:click="reverse">Reverse</button> -->
  <button @click="reverse">Reverse</button>
  <p v-for="data in datas" :key="data.id">{{data.name}}</p>
</template>
```

---

## v-model

```js
<script setup>
  import {ref} from 'vue';

  const msg = ref('some info here.');
  const isChecked = ref(true);
</script>

<template>
 <input type="text" v-model="msg"/>
 <input type="checkbox" v-model="isChecked"/>

 <p>msg: {{ msg }}</p>
 <p>isChecked: {{ isChecked }}</p>

 <button :disabled="!isChecked">save</button>
</template>
```

---
