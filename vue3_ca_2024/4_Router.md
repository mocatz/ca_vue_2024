---
marp: true
---

# Vue router

"This how to get different content from the URL"
"Manage the differents views (pages) in the application"

---

The router bind URL to compoents in views folder
Exercice:

1. Create 3 views (juste use `<template></template>`)

- Home page
- app page
- contact page

2. Use view router to manage these views
3. test in the browser by updating URL

---
