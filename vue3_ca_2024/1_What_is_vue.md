---
marp: true
---

# What_is_vue

Vue.js is an open-source model–view–viewmodel front end JavaScript library for **building user interfaces** and **single-page applications**. It was created by Evan You in 2014.
[wikipedia page](https://en.wikipedia.org/wiki/Vue.js)

---

## Some useful links

- [vue doc page](https://vuejs.org/) _the framework_
- [vite](https://vitejs.dev/) _JS build tool_
- [vue router](https://router.vuejs.org/) _the official router_
- [pinia](https://pinia.vuejs.org/) _state manager aka store_

---

## What is a SPA (_single page application_) ?

> An SPA (Single-page application) is a web app implementation that loads only a single web document, and then updates the body content of that single document via JavaScript APIs such as Fetch when different content is to be shown

_wikipedia_

---

Classic website have multiple pages with hardcoded dom.

SPA have one index.html and the body content is updated by JS

---
