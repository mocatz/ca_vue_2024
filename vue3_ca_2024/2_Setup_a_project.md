---
marp: true
---

# Setup a project

_You need to have nodeJS & npm installed: `node --version` `npm --version`_

- Open a terminal
- `npm create vue@latest`

---

✔ Project name: … <your-project-name>
✔ Add TypeScript? … **No** / Yes
✔ Add JSX Support? … **No** / Yes
✔ Add Vue Router for Single Page Application development? … No / **Yes**
✔ Add Pinia for state management? … No / **Ye**s
✔ Add Vitest for Unit testing? … No / **Yes**
✔ Add an End-to-End Testing Solution? … **No** / Cypress / Playwright
✔ Add ESLint for code quality? … No / **Yes**
✔ Add Prettier for code formatting? … No / **Yes**

Scaffolding project in ./<your-project-name>...
Done.

---

```
cd vue-project
npm install # Install all needed dependencies
npm run lint
npm run dev # Run a local server in dev mode
```

---

## Add Vue devtool

[Vue devtool](https://chromewebstore.google.com/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd)

---
