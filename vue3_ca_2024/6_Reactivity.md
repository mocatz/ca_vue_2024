---
marp: true
---

## Reactivity

Ref / Computed / Watch

---

```js

<script setup>
  import {computed, ref, watch} from 'vue';

  const msg = ref('some info here.');
  const isChecked = ref(true);
  const isSuperPass = ref(false);

  const isUserCanSave = computed(() => {
    return isChecked.value && msg.trim().length > 1;
  })

  watch(msg, (nVal, oval) => {
    if (nVal == 'superpass') {
      isSuperPass.value = true;
    }
  })
</script>

<template>
 <input type="text" v-model="msg"/>
 <input type="checkbox" v-model="isChecked"/>

 <p>msg: {{ msg }}</p>
 <p>isChecked: {{ isChecked }}</p>

 <button :disabled="!isChecked">save</button>
 <p v-if="isSuperPass">Super pass</p>
</template>
```
