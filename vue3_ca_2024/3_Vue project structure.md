---
marp: true
---

# Vue project structure

---

- node modules
- public
- src _(Your working folder)_
  - assets _(All images, global css files)_
  - components _(The main work is here)_
  - router _(the routing settings)_
  - store _(Pinia stores)_
  - views _(The "pages" components managed by the router)_
  - App.vue (3) _(Vue Application entry point)_
  - main.js (2) _(vue app settings and global imports)_
- index.html (1)
- package.json _(all dependencies and scripts)_
