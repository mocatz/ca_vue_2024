## exo 1 : To to list

- An input field to add a task title
- A textfield to add a task description
- A btns to set weight min: 0, max: 5
- A button to add task to the list
- Display task
  - Task text
  - weight (optional: Set different colors for height )
  - a btn done that change the style of the task
  - a btn delete task
- Add Select to sort
  - by timestamp
  - by weight
  - done / toDo
- On click task title display task info(title/description/weight / created / done / time elapsed)

## Exo 2 : cities

- Use the node server created
- import city file data
- Fetch data in a vue app
- display cities list
- Add select with dept list + 'all dept'
- On selct dept change display only the matched cities
- Add a component to display the number of matched cities
- Add a seach field to filter the match citie name

## Exo 3

- [Visual Reference](https://www.frontendmentor.io/challenges/frontend-quiz-app-BE7xkzXQnU)
- [Question ref](https://www.w3schools.com/html/html_quiz.asp)
- Create an API endpoint to save the user mail and the score
- Create a dashboard view
- Stats counter per quiz
- Max / min score per quiz
- Chart with answer per question
- List all submit (email quiz score)
