---
marp: true
---

# VUE3

Composition API / setup /

---

## Summary 1/3

1. What Is Vue
2. Setup a project
3. Vue project structure
4. Router

---

5. Components (Composition API / Option API)
6. Reactivity (ref/computed/watch)
7. Directives (v-bind, v-if, v-show, v-for, v-on, v-model)
8. import and use a library (axios)

---

9. Pinia Store
