---
marp: true
---

# Components

A component is a file that represent a section of a web page with the UI(html/css) and Script.
It allow to have **small**, **reusable** elements and section.
The Art of creating components is the art of **split** view and elements

- [Atomic design](https://atomicdesign.bradfrost.com/chapter-2/)
  Vue get two way (API) of writting components:

- Option API (common in vue 1/2/3)
- Composition API (Vue 3)

---

## Option API

```js
<script>
export default {
    data() {
        return {
            name: '',
            age: 0,
        }
    },
    computed: {
        displayProfile() {
         return `My name is ${this.name} and i am ${this.age}`;
        }
    },
    methods: {
        isUserCanDrive(age) {
          return age >= 18;
        }
    },
    mounted() {
        console.log('Application mounted');
    },
}
</script>
```

---

## Composition API (setup)

```js
<script setup>
import {computed, onMounted, ref } from 'vue';

const name = '';
const age = 0;

const displayProfile = computed(() => `My name is ${this.name} and i am ${this.age}`)

function isUserCanDrive(age) {
  return age >= 18;
}

onMounted(() => {
  console.log('Application mounted');
})
</script>
```

---

## vue component structure

```js
<script>
  // -- All vue/js code here
</script>

<template>
  <--! All html here -->
</template>

<style scoped>
  /* All css style here */
  /* attribute scoped mean the css is only applicable to this component*/
</style>
```

---

## Parent / child components

Components act as tree structure: From parent to child.

```js
<script setup>
import Child from './Child.vue'
</script>

<template>
  <p>Parent component</p>
  <Child />
</template>
```

```js
<template>
  <p>I a child componet</p>
</template>
```

---

## Pass data from parent to child (Props)

(Parent component)

```js
<script setup>
import {ref} from 'vue';
import Child from './Child.vue';
const msg = ref('Hi');
</script>

<template>
  <p>Parent component</p>
  <Child :msg="msg"/>
</template>
```

---

Pass data from parent to child (Props) (Child component)

```js
<script setup>
  const props = defineProps({
    msg: {
      type: string,
      default: 'No message',
    }
  })
</script>
<template>
  <p>{{ props.msg }}</p>
</template>
```

---

## Pass data from child to parent

```js
<script setup>
import {ref} from 'vue';
import Child from './Child.vue';

const msg = ref('Hi');

function updateMsg(newMsg) {
  msg.value = newMsg;
}
</script>

<template>
  <p>Parent component: {{ msg }}</p>
  <Child @emitMessage="updateMsg"/>
</template>
```

---

```js
<script setup>
  import {ref} from 'vue';
  const emits = defineEmits(['emitMessage']);
  const msg = ref('')
  function emitNewMessage(msgToEmit) {
    emits('emitMessage', msgToEmit);
  }
</script>
<template>
  <input v-model="msg"/>
  <button @click="emitNewMessage(msg)">emit to parent</button>
</template>
```

---

## Bind Data parent / child

```js
<script setup>
import {ref} from 'vue';
import Child from './Child.vue';

const msgParent = ref('Hi');
</script>

<template>
  <p>Parent component: {{ msgParent }}</p>
  <Child v-model:msg="msgParent"/>
</template>
```

---

```js
<script setup>
  import {ref} from 'vue';
   const props = defineProps({
    msg: {
      type: string,
      default: 'No message',
    }
  })
  const currMsg = ref(props.msg);
  const emits = defineEmits(['update:msg']);
</script>
<template>
  <input v-model="currMsg" @keyup="emits('update:msg', currMsg)"/>
</template>
```

---

## Use slot

A common card component

---

## Component lifecycle

[vue lifecycle](https://vuejs.org/guide/essentials/lifecycle.html)
